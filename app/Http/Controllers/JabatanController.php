<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;

class JabatanController extends Controller
{

    public function data(){
       $jabatan = Jabatan::all();

       return response()->json($jabatan);
    }

    public function edit($id){
       $jabatan = Jabatan::find($id);

       return response()->json($jabatan);
    }

    public function store(Request $request)
    {

        $jabatan = new Jabatan;
        $jabatan->nama_jabatan = $request['nama_jabatan'];
        $jabatan->save();

        return response()->json($jabatan);   
    }

    public function update(Request $request, $id)
    {
        
        $jabatan = Jabatan::find($id);
        $jabatan->nama_jabatan = $request['nama_jabatan'];
        $jabatan->update();

        return response()->json($jabatan);
    }

    public function destroy($id)
    {
        $jabatan = Jabatan::find($id);
        $jabatan->delete();

        return response()->json($jabatan); 
    }

}
