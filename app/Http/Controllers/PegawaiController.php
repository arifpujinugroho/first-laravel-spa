<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\Jabatan;

class PegawaiController extends Controller
{

    public function data(Request $request){
       $pegawai = Pegawai::leftJoin('jabatan', 'jabatan.id_jabatan', '=', 'pegawai.id_jabatan')->get();

       return response()->json($pegawai);
    }

    public function create(){
       $jabatan = Jabatan::all();

       return response()->json($jabatan);
    }

    public function edit($id){
       $pegawai = Pegawai::find($id);
       $jabatan = Jabatan::all();

       return response()->json(['pegawai'=> $pegawai, 'jabatan'=>$jabatan]);
    }

    public function store(Request $request)
    {

       if($request->hasfile('foto'))
        {
            $file = $request->file('foto');
            $namafile=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $namafile);
        }
      
        $pegawai = new Pegawai;
        $pegawai->nama_pegawai = $request['nama_pegawai'];
        $pegawai->jenis_kelamin = $request['jenis_kelamin'];
        $pegawai->tgl_lahir = $request['tgl_lahir'];
        $pegawai->id_jabatan = $request['id_jabatan'];
        $pegawai->keterangan = $request['keterangan'];
        $pegawai->foto = $namafile;
        $pegawai->save();
        
       return response()->json($request);
    }

    public function update(Request $request, $id)
    {
        $ubahfile = false;
        if($request->hasfile('foto'))
        {
            $file = $request->file('foto');
            $namafile=time().$file->getClientOriginalName();
            $file->move(public_path().'/images/', $namafile);
            $ubahfile = true;
        }

        $pegawai = Pegawai::find($id);        
        $pegawai->nama_pegawai = $request['nama_pegawai'];
        $pegawai->jenis_kelamin = $request['jenis_kelamin'];
        $pegawai->tgl_lahir = $request['tgl_lahir'];
        $pegawai->id_jabatan = $request['id_jabatan'];
        $pegawai->keterangan = $request['keterangan'];
        if($ubahfile){
           if(file_exists(public_path().'/images/'.$pegawai->foto)) unlink(public_path().'/images/'.$pegawai->foto);
           $pegawai->foto = $namafile; 
        } 
        $pegawai->update();

       return response()->json($request);
    }

    public function destroy($id)
    {
        $pegawai = Pegawai::find($id);
        if(file_exists(public_path().'/images/'.$pegawai->foto)) unlink(public_path().'/images/'.$pegawai->foto);
        $pegawai->delete();

       return response()->json($pegawai);
    }
}
