<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Login Aplikasi</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
   <body class="h-100 bg-info d-flex align-items-center">
     <div class="container">
       <div class="row">
         <div class="col-sm-6 col-md-4 mx-auto bg-light p-4">
            <h3 class="text-center text-info pb-3 mb-3 border-bottom">Login Aplikasi</h3>

            <form method="post" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group p-0 {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control form-control-lg mb-3" placeholder="Email" name="email" autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

               <div class="form-group p-0 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control form-control-lg mb-3" placeholder="Password" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
               
               <input class="btn btn-info btn-lg btn-block" type="submit" value="Login">
            </form>
         </div>
       </div>
     </div>
   </body>
</html>
