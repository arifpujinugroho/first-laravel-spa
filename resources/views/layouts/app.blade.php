<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Manajemen Pegawai</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('open-iconic/css/open-iconic-bootstrap.min.css') }}" rel="stylesheet">
</head>
  <body class="h-100">

    <div id="app">
      <app></app>
    </div>

    <script>
      window.Laravel = <?php echo json_encode([
          'csrfToken' => csrf_token(),
        ]);  ?>;
    </script>

    <script src="{{ asset('js/app.js') }}"></script>
   </body>
</html>