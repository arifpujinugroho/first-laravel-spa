<p align="center"><a href="https://laravel.com" target="_blank"><img width="230" src="https://laravel.com/assets/img/components/logo-laravel.svg" alt="Logo Laravel"></a></p>
<br>

> **Remember!!** this project for learning **LARAVEL Single Page Application**.
<h1>First Laravel Single Page Application</h1>
<br>
<h2>Gambar Screnshoot :</h2>
<img src="public/images/screenshot-web.png" alt="SS Dashboard"/>

<br>
<h2>Cara install project ini :</h2>
<ul>
  <li><p>1. Install GIT <code><a href="https://git-scm.com/" target="_blank">https://git-scm.com/</a></code></p></li>
  <li><p>2. Install Composer <code><a href="https://getcomposer.org/" target="_blank">https://getcomposer.org/</a></code></p></li>
  <li><p>3. Install NPM atau NodeJs <code><a href="https://nodejs.org/" target="_blank">https://nodejs.org/</a></code></p></li>
  <li><p>4. Ketikan di Git Bash/CMD/Terminal/PowerShell <code>git clone https://gitlab.com/arifpujinugroho/first-laravel-spa.git</code></p></li>
  <li><p>5. Masuk ke folder projectnya atau ketikan <code>cd first-laravel-spa</code></p></li>
  <li><p>6. Ketikan<code>composer update</code></p></li>
  <li><p>7. Duplikat .env.example dan Ubah nama file duplikat tersebut menjadi .env atau ketikan <code>cp .env.example .env</code></p></li>
  <li><p>8. Ketikan<code>php artisan key:generate</code></p></li>
  <li><p>9. Masukan semua nama database, username database, dan password database.</p></li>
  <li><p>10. Migrate database setelah diatur, atau ketikan <code>php artisan migrate</code> jika sudah ada table dalam databse maka <code>php artisan migrate:fresh</code>.</p></li>
  <li><p>11. Install Vue and Vue Route, atau ketikan <code>npm install</code></p></li>
  <li><p>12. Running menggunakan <code>php artisan serve</code></li>
  <li><p>13. <strong>Jangan Lupa untuk syukuri apapun</strong> yang terjadi pada WEB ini 😊</p></li>
</ul>


<h2>What's Up</h2>
<ul>
    <li>Jan 16, 2020 - (<a href="https://gitlab.com/arifpujinugroho" target="_blank">Arif Puji Nugroho</a>) try make multi components Vue with Laravel</li>
</ul>

<h2>Credits</h2>
<ul>
    <li><a href="https://github.com/laravel/laravel" target="_blank">Laravel Framework Web</a></li>
    <li><a href="#!" target="_blank">PHP 7 in 1 Web Programing Tingkat Lanjut</a></li>
</ul>
